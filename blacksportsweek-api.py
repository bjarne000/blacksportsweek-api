import os

from flask import Flask, flash, request, redirect, url_for, render_template, jsonify
from werkzeug.utils import secure_filename
from flask_mysqldb import MySQL

# vars
PATH_TO_FOLDER = "/home/blacksportweek/blacksportsweek-api/uploads/"
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'svg'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = PATH_TO_FOLDER
app.config['MYSQL_HOST'] = '144.76.225.202'
app.config['MYSQL_USER'] = 'blacksportsweek'
app.config['MYSQL_PASSWORD'] = 'xLmio12dr'
app.config['MYSQL_DB'] = 'blacksportsweek'

#db init
mysql = MySQL(app)

@app.route('/')
def home():
    return render_template('home.html')

# html routes
@app.route('/form')
def up_form():
    return render_template("upload-form.html")

@app.route("/end")
def end():
    return render_template("end-screen.html")

@app.route("/create-tile")
def create_tile():
    return render_template("create-tile.html")


## file upload
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['POST'])
def upload_files():
    if request.method == 'POST':
        if 'file[]' not in request.files:
            flash('No file part')
            return redirect(request.url)
        files = request.files.getlist('file[]')

        for file in files:
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        flash('File(s) successfully uploaded')
        return redirect('/end')

@app.route('/create', methods=['POST'])
def create():
    if request.method == "POST":
        link = request.form.get('link')
        title = request.form.get('title')
        imageTile = request.form.get('imageTile')
        imageFull = request.form.get('imageFull')
        imageLogo = request.form.get('imageLogo')
        imageLogoNeg = request.form.get('imageLogo')
        description = request.form.get('description')
        sortOrder = request.form.get('sortOrder')
        isActive = request.form.get('isActive')

        try:
            cur = mysql.connection.cursor()
            cur.execute("INSERT INTO Tile(link, title, imageTile, imageFull, imageLogo, imageLogoNeg, description, sortOrder, isActive) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                        (link, title, imageTile, imageFull, imageLogo, imageLogoNeg, description, sortOrder, isActive))
            mysql.connection.commit()
            cur.close()
            return 'success'
        except Exception as e:
            print(e)
        return redirect('/')


@app.route('/active/all-tiles', methods=['GET'])
def get_tiles():
    cursor = mysql.connection.cursor()
    cursor.execute("SELECT * FROM Tile;")
    result = cursor.fetchall()

    json_result = []
    for x in result:
        d = {'id': x[0], 'link': x[1], 'title': x[2], 'imageTile': x[3],
             'imageFull': x[4], 'imageLogo': x[5], 'imageLogoNeg': x[6],
             'description': x[7], 'sortOrder': x[8], 'isActive': x[9]}
        json_result.append(d)

    return jsonify(json_result)

if __name__ == '__main__':
    app.secret_key = "super secret key"
    app.run('0.0.0.0', port=2001)
